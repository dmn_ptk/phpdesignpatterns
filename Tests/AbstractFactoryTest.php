<?php
declare(strict_types=1);
namespace PhpDesignPatterns\Tests;

use PhpDesignPatterns\Creational\AbstractFactory\AbstractFactory;
use PhpDesignPatterns\Creational\AbstractFactory\BurgerClassic;
use PhpDesignPatterns\Creational\AbstractFactory\PizzaFactory;
use PhpDesignPatterns\Creational\AbstractFactory\BurgerFactory;
use PhpDesignPatterns\Creational\AbstractFactory\PizzaMargherita;
use PHPUnit\Framework\TestCase;

class AbstractFactoryTest extends TestCase
{
    public function testGetFactory() : void
    {
        $this->assertInstanceOf(PizzaFactory::class, AbstractFactory::getFactory(AbstractFactory::PIZZA));
        $this->assertInstanceOf(BurgerFactory::class, AbstractFactory::getFactory(AbstractFactory::BURGER));
    }

    public function testGetMenuItem() : void
    {
        $pizzaFactory = AbstractFactory::getFactory(AbstractFactory::PIZZA);
        $this->assertInstanceOf(PizzaMargherita::class, $pizzaFactory->getMenuItem(PizzaFactory::MARGHERITA));

        $burgerFactory = AbstractFactory::getFactory(AbstractFactory::BURGER);
        $this->assertInstanceOf(BurgerClassic::class, $burgerFactory->getMenuItem(BurgerFactory::CLASSIC));
    }
}
