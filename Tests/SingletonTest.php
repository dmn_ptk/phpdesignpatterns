<?php
declare(strict_types=1);
namespace PhpDesignPatterns\Tests;

use PhpDesignPatterns\Creational\Singleton\Preferences;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass PhpDesignPatterns\Creational\Singleton
 */
class SingletonTest extends TestCase
{
    /**
     * @covers ::getInstance
     */
    public function testGetInstanceFirstTime(): void
    {
        $this->assertInstanceOf(Preferences::class, Preferences::getInstance());
    }

    /**
     * @covers ::getInstance
     */
    public function testGetInstanceTwoTimes(): void
    {
        $this->assertInstanceOf(Preferences::class, Preferences::getInstance());
        $this->assertInstanceOf(Preferences::class, Preferences::getInstance());
    }

    /**
     * @expectedException \Error
     * @covers ::__construct
     */
    public function testConstructor() : void
    {
        $this->assertNotInstanceOf(Preferences::class, new Preferences());
    }

    /**
     * @covers ::setOption
     * @covers ::getOption
     */
    public function testSetOption() : void
    {
        $preferences = Preferences::getInstance();
        $this->assertNull($preferences->setOption('size', 100));
        $this->assertEquals(100, $preferences->getOption('size'));
    }
}
