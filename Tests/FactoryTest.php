<?php
declare(strict_types=1);
namespace PhpDesignPatterns\Tests;

use PhpDesignPatterns\Creational\Factory\PizzaCheese;
use PhpDesignPatterns\Creational\Factory\PizzaFactory;
use PhpDesignPatterns\Creational\Factory\PizzaMargherita;
use PHPUnit\Framework\TestCase;

class FactoryTest extends TestCase
{
    public function testGetMenuItem(): void
    {
        $this->assertInstanceOf(PizzaMargherita::class, PizzaFactory::getMenuItem(PizzaFactory::MARGHERITA));
        $this->assertInstanceOf(PizzaCheese::class, PizzaFactory::getMenuItem(PizzaFactory::CHEESE));
    }

    /**
     * @expectedException ArgumentCountError
     */
    public function testGetMenuItemNoArg(): void
    {
        $this->assertNull(PizzaFactory::getMenuItem());

    }

    /**
     * @expectedException \Exception
     */
    public function testGetMenuItemNotExisting(): void
    {
        $this->assertNull(PizzaFactory::getMenuItem(123));

    }

    public function testGetName(): void
    {
        $margherita = PizzaFactory::getMenuItem(PizzaFactory::MARGHERITA);
        $this->assertEquals('Margherita', $margherita->getName());

        $cheese = PizzaFactory::getMenuItem(PizzaFactory::CHEESE);
        $this->assertEquals('Cheese', $cheese->getName());
    }
}
