<?php
declare(strict_types=1);

use PhpDesignPatterns\Structural\Proxy\ProxyService;
use PHPUnit\Framework\TestCase;

class ProxyServiceTest extends TestCase
{
    public function testGenerateReport(): void
    {
        $remote = new class() implements \PhpDesignPatterns\Structural\Proxy\Service{
            public function generateReport(): array
            {
                return ['result' => []];
            }
        };

        $proxy = new ProxyService($remote);
        $this->assertArrayHasKey('result', $proxy->generateReport());
    }
}
