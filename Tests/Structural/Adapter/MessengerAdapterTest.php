<?php
declare(strict_types=1);

use PhpDesignPatterns\Structural\Adapter\MessengerAdapter;
use PHPUnit\Framework\TestCase;

class MessengerAdapterTest extends TestCase
{
    public function testShowMessage(): void
    {
        $messenger = new MessengerAdapter(new \PhpDesignPatterns\Structural\Adapter\NewMessenger());
        $this->assertEquals('Some message in an array', $messenger->showMessage());
    }
}
