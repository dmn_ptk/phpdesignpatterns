<?php
declare(strict_types=1);
namespace PhpDesignPatterns\Tests;

use PhpDesignPatterns\Creational\Prototype\BookProduct;
use PhpDesignPatterns\Creational\Prototype\ProductFactory;
use PHPUnit\Framework\TestCase;

class PrototypeTest extends TestCase
{
    public function testClone() : void
    {
        $book1 = new BookProduct();
        $book1->setTitle('Abc');
        $book1clone = clone $book1;

        $this->assertInstanceOf(BookProduct::class, $book1clone);
        $this->assertEquals('Abc', $book1clone->getTitle());
    }

    public function testGetProduct() : void
    {
        $book1 = new BookProduct();
        $book1->setTitle('Abc');
        $book2 = new BookProduct();
        $book2->setTitle('Def');
        $productFactory = new ProductFactory(['abc' => $book1, 'def' => $book2]);

        $book1clone = $productFactory->getProduct('abc');
        $this->assertInstanceOf(BookProduct::class, $book1clone);
        $this->assertEquals('Abc', $book1clone->getTitle());
    }

    /**
     * @expectedException Exception
     */
    public function testGetNotExistingProduct() : void
    {
        $book1 = new BookProduct();
        $book1->setTitle('Abc');
        $book2 = new BookProduct();
        $book2->setTitle('Def');
        $productFactory = new ProductFactory(['abc' => $book1, 'def' => $book2]);

        $this->assertNull($productFactory->getProduct());

    }
}
