<?php
declare(strict_types=1);


use PhpDesignPatterns\Behavioral\Memento\Caretaker;
use PhpDesignPatterns\Behavioral\Memento\CounterMemento;
use PHPUnit\Framework\TestCase;

class CaretakerTest extends TestCase
{
    public function testGet(): void
    {
        // given
        $caretaker = new Caretaker();

        /** @var CounterMemento $memento */
        $memento = $this->createMock(CounterMemento::class);

        // when
        $caretaker->add($memento);

        //then
        $this->assertInstanceOf(CounterMemento::class, $caretaker->get(0));
    }

    /**
     * @expectedException \Exception
     */
    public function testGetIndexOutOfRange(): void
    {
        // given
        $caretaker = new Caretaker();

        //when, then
        $caretaker->get(0);
    }
}
