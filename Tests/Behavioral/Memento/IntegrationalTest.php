<?php
declare(strict_types=1);


use PhpDesignPatterns\Behavioral\Memento\Caretaker;
use PhpDesignPatterns\Behavioral\Memento\Counter;
use PHPUnit\Framework\TestCase;

class IntegrationalTest extends TestCase
{
    public function testMain()
    {
        // given
        $counter = new Counter(0);
        $caretaker = new Caretaker();

        // when
        $caretaker->add($counter->createMemento());
        $counter->count();
        $caretaker->add($counter->createMemento());
        $counter->count();

        $counter->setMemento($caretaker->get(0));
        $state1 = $counter->getState();
        $counter->setMemento($caretaker->get(1));
        $state2 = $counter->getState();

        //then
        $this->assertEquals(0, $state1);
        $this->assertEquals(1, $state2);
    }
}
