<?php
declare(strict_types=1);


use PhpDesignPatterns\Behavioral\Memento\Counter;
use PhpDesignPatterns\Behavioral\Memento\CounterMemento;
use PHPUnit\Framework\TestCase;

class CounterTest extends TestCase
{
    public function testGetState(): void
    {
        // given
        $couter = new Counter(0);

        //then
        $this->assertEquals(0, $couter->getState());
    }

    /**
     * @depends testGetState
     */
    public function testCount(): void
    {
        // given
        $counter = new Counter(0);

        // when
        $counter->count();

        //then
        $this->assertEquals(1, $counter->getState());
    }

    public function testCreateMemento(): void
    {
        // given
        $counter = new Counter(0);

        // when
        $memento = $counter->createMemento();

        //then
        $this->assertInstanceOf(CounterMemento::class, $memento);
    }

    /**
     * @depends testCreateMemento
     */
    public function testSetMemento(): void
    {
        // given
        $counter = new Counter(0);

        // when
        $memento = $counter->createMemento();
        $counter->count();
        $stateAfterCount = $counter->getState();

        $counter->setMemento($memento);
        $stateAfterSetMemento = $counter->getState();

        //then
        $this->assertEquals(1, $stateAfterCount);
        $this->assertEquals(0, $stateAfterSetMemento);
    }
}
