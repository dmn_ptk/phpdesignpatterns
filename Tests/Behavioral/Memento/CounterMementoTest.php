<?php
declare(strict_types=1);


use PhpDesignPatterns\Behavioral\Memento\CounterMemento;
use PHPUnit\Framework\TestCase;

class CounterMementoTest extends TestCase
{
    public function testGetState(): void
    {
        $memento = new CounterMemento(10);
        $this->assertEquals(10, $memento->getState());
    }
}
