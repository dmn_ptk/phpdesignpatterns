<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Strategy\PrintFullStrategy;
use PhpDesignPatterns\Behavioral\Strategy\Book;
use PHPUnit\Framework\TestCase;

class PrintFullStrategyTest extends TestCase
{
    public function testExecute(): void
    {
        $book = new Book([
            'title' => 'Lord of Rings',
            'author' => 'J. R. R. Tolkien',
            'publisher' => 'Abc Publishing',
            'year' => '2018'
        ]);

        $strategy = new PrintFullStrategy();
        $strategy->setBook($book);
        $this->assertEquals('J. R. R. Tolkien: Lord of Rings, Abc Publishing, 2018', $strategy->execute());
    }

}
