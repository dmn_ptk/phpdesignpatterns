<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Strategy\BookList;
use PhpDesignPatterns\Behavioral\Strategy\Book;
use PhpDesignPatterns\Behavioral\Strategy\PrintFullStrategy;
use PHPUnit\Framework\TestCase;

class BookListTest extends TestCase
{
    public function testSetBooks(): BookList
    {
        $bookList = new BookList();
        $bookList->setBooks([
            new Book([
                'title' => 'Lord of Rings 1',
                'author' => 'J. R. R. Tolkien',
                'publisher' => 'Abc Publishing',
                'year' => '2018'
            ]),
            new Book([
                'title' => 'Lord of Rings 2',
                'author' => 'J. R. R. Tolkien',
                'publisher' => 'Abc Publishing',
                'year' => '2018'
            ]),
            new Book([
                'title' => 'Lord of Rings 3',
                'author' => 'J. R. R. Tolkien',
                'publisher' => 'Abc Publishing',
                'year' => '2018'
            ]),
        ]);

        $this->assertEquals(3, $bookList->count());
        return $bookList;
    }

    /**
     * @depends testSetBooks
     * @return BookList
     */
    public function testSetStrategy(BookList $bookList): BookList
    {
        $bookList->setStrategy(new PrintFullStrategy());
        $this->assertInstanceOf(PrintFullStrategy::class, $bookList->getStrategy());
        return $bookList;
    }

    /**
     * @depends testSetStrategy
     * @param BookList $bookList
     */
    public function testReturnList(BookList $bookList): void
    {
        $this->assertCount(3, $bookList->returnList());
    }
}
