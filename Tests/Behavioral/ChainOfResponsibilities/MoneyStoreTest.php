<?php


use PhpDesignPatterns\Behavioral\ChainOfResponsibilities\MoneyBundle;
use PhpDesignPatterns\Behavioral\ChainOfResponsibilities\MoneyStore;
use PhpDesignPatterns\Behavioral\ChainOfResponsibilities\Money;
use PHPUnit\Framework\TestCase;

class MoneyStoreTest extends TestCase
{
    public function testSetNextMoneyStore(): MoneyStore
    {


        $moneyStore = new MoneyStore(new Money([
            'value' => 200,
            'currency' => 'PLN'
        ]));
        $this->assertFalse($moneyStore->hasLink());

        $moneyStore->setNextLink(new MoneyStore(new Money([
            'value' => 100,
            'currency' => 'PLN'
        ])));
        $this->assertTrue($moneyStore->hasLink());
        return $moneyStore;
    }

    /**
     * @depends testSetNextMoneyStore
     */
    public function testRelease100(MoneyStore $moneyStore)
    {
        $moneyBundle = $moneyStore->release(new MoneyBundle(100, 'PLN'));
        $this->assertInstanceOf(MoneyBundle::class, $moneyBundle);
        $bundle = $moneyBundle->getBundle();
        $this->assertCount(1, $bundle);
        $this->assertEquals(100, $bundle[0]->value);
        $this->assertEquals(1, $bundle[0]->count);
    }

    /**
     * @depends testSetNextMoneyStore
     */
    public function testRelease300(MoneyStore $moneyStore)
    {
        $moneyBundle = $moneyStore->release(new MoneyBundle(300, 'PLN'));
        $this->assertInstanceOf(MoneyBundle::class, $moneyBundle);
        $bundle = $moneyBundle->getBundle();
        $this->assertCount(2, $bundle);
        $this->assertEquals(200, $bundle[0]->value);
        $this->assertEquals(1, $bundle[0]->count);
        $this->assertEquals(100, $bundle[1]->value);
        $this->assertEquals(1, $bundle[1]->count);
    }

    /**
     * @depends testSetNextMoneyStore
     */
    public function testRelease400(MoneyStore $moneyStore)
    {
        $moneyBundle = $moneyStore->release(new MoneyBundle(400, 'PLN'));
        $this->assertInstanceOf(MoneyBundle::class, $moneyBundle);
        $bundle = $moneyBundle->getBundle();
        $this->assertCount(1, $bundle);
        $this->assertEquals(200, $bundle[0]->value);
        $this->assertEquals(2, $bundle[0]->count);
    }

    /**
     * @depends testSetNextMoneyStore
     */
    public function testRelease500(MoneyStore $moneyStore)
    {
        $moneyBundle = $moneyStore->release(new MoneyBundle(500, 'PLN'));
        $this->assertInstanceOf(MoneyBundle::class, $moneyBundle);
        $bundle = $moneyBundle->getBundle();
        $this->assertCount(2, $bundle);
        $this->assertEquals(200, $bundle[0]->value);
        $this->assertEquals(2, $bundle[0]->count);
        $this->assertEquals(100, $bundle[1]->value);
        $this->assertEquals(1, $bundle[1]->count);
    }
}
