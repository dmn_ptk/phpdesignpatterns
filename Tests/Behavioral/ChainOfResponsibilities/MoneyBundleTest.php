<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\ChainOfResponsibilities\Money;
use PhpDesignPatterns\Behavioral\ChainOfResponsibilities\MoneyBundle;
use PHPUnit\Framework\TestCase;


class MoneyBundleTest extends TestCase
{
    public function testGetMoneyToRelease(): MoneyBundle
    {
        $moneyBundle = new MoneyBundle(300, 'PLN');
        $this->assertEquals(300, $moneyBundle->getMoneyToRelease());
        return $moneyBundle;
    }

    /**
     * @depends testGetMoneyToRelease
     */
    public function testAdd(MoneyBundle $moneyBundle): MoneyBundle
    {
        $moneyBundle->add(new Money([
            'value' => 200,
            'count' => 1,
            'currency' => 'PLN'
        ]));

        $this->assertEquals(100, $moneyBundle->getMoneyToRelease());
        return $moneyBundle;
    }

    /**
     * @depends testAdd
     */
    public function testGetBundle(MoneyBundle $moneyBundle): void
    {
        $this->assertCount(1, $moneyBundle->getBundle());

        $moneyBundle->add(new Money([
            'value' => 100,
            'count' => 1,
            'currency' => 'PLN'
        ]));

        $this->assertCount(2, $moneyBundle->getBundle());
    }
}
