<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\SleepCard;
use PhpDesignPatterns\Behavioral\Command\SleepEntity;
use PHPUnit\Framework\TestCase;

class SleepCardTest extends TestCase
{
    public function testExecute(): void
    {
        $card = new SleepCard(new SleepEntity(60));
        $array = $card->execute();
        $this->assertArrayHasKey('class', $array);
        $this->assertArrayHasKey('desc', $array);
    }
}
