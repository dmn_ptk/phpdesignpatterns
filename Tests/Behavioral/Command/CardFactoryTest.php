<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\CardFactory;
use PhpDesignPatterns\Behavioral\Command\Entity;
use PhpDesignPatterns\Behavioral\Command\SleepCard;
use PhpDesignPatterns\Behavioral\Command\SleepEntity;
use PHPUnit\Framework\TestCase;

class FakeEntity implements Entity{}

class CardFactoryTest extends TestCase
{
    public function testReturnCard(): void
    {
        $this->assertInstanceOf(SleepCard::class, CardFactory::returnCommand(new SleepEntity(60)));
    }

    /**
     * @expectedException Exception
     */
    public function testReturnCardNoFile(): void
    {
        CardFactory::returnCommand(new FakeEntity());
    }
}
