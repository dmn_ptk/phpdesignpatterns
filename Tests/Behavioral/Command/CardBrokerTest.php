<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\Card;
use PhpDesignPatterns\Behavioral\Command\CardBroker;
use PhpDesignPatterns\Behavioral\Command\Entity;
use PHPUnit\Framework\TestCase;

class CardBrokerTest extends TestCase
{
    public function testAddCard(): CardBroker
    {
        $card = new class(new class() implements Entity{}) extends Card{
            public function execute(){
                return [
                    'desc' => 'test',
                    'class' => 'fake'
                ];
            }
        };
        
        $cardBroker = new CardBroker();
        $cardBroker->add($card);
        $this->assertEquals(1, $cardBroker->count());
        return $cardBroker;
    }

    /**
     * @depends testAddCard
     */
    public function testReturnProcessedCards(CardBroker $cardBroker): void
    {
        $this->assertCount(1, $cardBroker->returnProcessed());
    }
}
