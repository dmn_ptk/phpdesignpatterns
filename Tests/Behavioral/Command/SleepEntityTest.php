<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\SleepEntity;
use PHPUnit\Framework\TestCase;

class SleepEntityTest extends TestCase
{
    public function testGetDuration(): void
    {
        $sleep = new SleepEntity(60);
        $this->assertEquals(60, $sleep->getDuration());
    }
}
