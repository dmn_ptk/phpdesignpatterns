<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\NappyEntity;
use PHPUnit\Framework\TestCase;

class NappyEntityTest extends TestCase
{
    public function testGetType(): void
    {
        $nappy = new NappyEntity(NappyEntity::WET);
        $this->assertEquals(NappyEntity::WET, $nappy->getType());
    }
}
