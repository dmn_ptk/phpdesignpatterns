<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\NappyCard;
use PhpDesignPatterns\Behavioral\Command\NappyEntity;
use PHPUnit\Framework\TestCase;

class NappyCardTest extends TestCase
{
    public function testExecute(): void
    {
        $card = new NappyCard(new NappyEntity(NappyEntity::WET));
        $array = $card->execute();
        $this->assertArrayHasKey('class', $array);
        $this->assertArrayHasKey('desc', $array);
    }
}
