<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Command\CardBroker;
use PhpDesignPatterns\Behavioral\Command\CardFactory;
use PhpDesignPatterns\Behavioral\Command\CommandService;
use PhpDesignPatterns\Behavioral\Command\NappyEntity;
use PhpDesignPatterns\Behavioral\Command\SleepEntity;
use PHPUnit\Framework\TestCase;

class CardServiceTest extends TestCase
{
    public function testAddElementsFromEntities(): CommandService
    {
        $cardService = new CommandService();
        $cardService->setBroker(new CardBroker());
        $cardService->setFactory(new CardFactory());
        $cardService->addElementsFromEntities([
            new SleepEntity(60),
            new NappyEntity(NappyEntity::WET),
            new SleepEntity(30)
        ]);

        $this->assertEquals(3, $cardService->count());

        return $cardService;
    }

    /**
     * @depends testAddElementsFromEntities
     */
    public function testReturnProcessedCards(CommandService $cardService): void
    {
        $this->assertCount(3, $cardService->returnProcessedCommands());
    }
}
