<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Mediator\Client;
use PhpDesignPatterns\Behavioral\Mediator\ConcreteClient;
use PhpDesignPatterns\Behavioral\Mediator\Mediator;
use PhpDesignPatterns\Behavioral\Mediator\Request;
use PhpDesignPatterns\Behavioral\Mediator\Response;
use PHPUnit\Framework\TestCase;

class ConcreteClientTest extends TestCase
{
    public function testSendRequestAndGetResponse()
    {
        $mediator = new class() implements Mediator {
            public function getResponse(Request $request): Response
            {
                return new Response([
                    'senderType' => 'B',
                    'receiverType' => 'A',
                    'data' => 'AB'
                ]);
            }

            public function setClient(Client $client): void{}
        };

        $client = new ConcreteClient('A');
        $client->setMediator($mediator);
        $client->sendRequest('B');
        $this->assertEquals('AB', ($client->getResponse())->data);
    }

    public function testSendResponse()
    {
        $client = new ConcreteClient('B');
        $this->assertEquals('AB', ($client->sentResponse(new Request([
            'senderType' => 'A',
            'receiverType' => 'B',
            'data' => 'A'
        ])))->data);
    }
}
