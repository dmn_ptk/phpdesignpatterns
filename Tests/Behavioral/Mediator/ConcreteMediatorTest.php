<?php
declare(strict_types=1);

use PhpDesignPatterns\Behavioral\Mediator\Client;
use PhpDesignPatterns\Behavioral\Mediator\ConcreteMediator;
use PhpDesignPatterns\Behavioral\Mediator\Mediator;
use PhpDesignPatterns\Behavioral\Mediator\Request;
use PhpDesignPatterns\Behavioral\Mediator\Response;
use PHPUnit\Framework\TestCase;

class ConcreteMediatorTest extends TestCase
{
    public function testSetClient()
    {
        $client = new class() implements Client{


            public function setMediator(Mediator $mediator): void{}

            public function sendRequest(string $receiverType): void{}

            public function sentResponse(Request $request): Response{
                return new Response();
            }

            public function getType(): string
            {
                return 'A';
            }
        };

        $mediator = new ConcreteMediator();
        $mediator->setClient($client);
        $this->assertEquals('A', ($mediator->getClient('A'))->getType());
    }

    /**
     * @expectedException OutOfRangeException
     */
    public function testGetClientOutOfRange()
    {
        $mediator = new ConcreteMediator();
        $mediator->getClient('A');
    }

    public function testGetResponse()
    {
        $client = new class() implements Client{


            public function setMediator(Mediator $mediator): void{}

            public function sendRequest(string $receiverType): void{}

            public function sentResponse(Request $request): Response{
                return new Response([
                    'senderType' => 'B',
                    'receiverType' => 'A',
                    'data' => 'AB'
                ]);
            }

            public function getType(): string
            {
                return 'B';
            }
        };

        $mediator = new ConcreteMediator();
        $mediator->setClient($client);
        $response = $mediator->getResponse(new Request([
            'senderType' => 'A',
            'receiverType' => 'B',
            'data' => 'A'
        ]));
        $this->assertEquals('AB', $response->data);
    }

    /**
     * @expectedException OutOfRangeException
     */
    public function testGetResponseOutOfRange()
    {
        $client = new class() implements Client{


            public function setMediator(Mediator $mediator): void{}

            public function sendRequest(string $receiverType): void{}

            public function sentResponse(Request $request): Response{
                return new Response();
            }

            public function getType(): string
            {
                return 'A';
            }
        };

        $mediator = new ConcreteMediator();
        $mediator->setClient($client);
        $response = $mediator->getResponse(new Request([
            'senderType' => 'A',
            'receiverType' => 'B',
            'data' => 'A'
        ]));
        $this->assertEquals('AB', $response->data);
    }
}
