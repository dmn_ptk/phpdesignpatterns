<?php
declare(strict_types=1);

namespace PhpDesignPatterns\Tests;

use PhpDesignPatterns\Structural\Composite\Directory;
use PHPUnit\Framework\TestCase;
use PhpDesignPatterns\Structural\Composite\File;

class CompositeTest extends TestCase
{
    public function testConstructFile(): void
    {
        $this->assertInstanceOf(File::class, new File('f1'));
    }

    /**
     * $depends testConstructFile
     */
    public function testFileGetContents(): void
    {
        $file1 = new File('track1.mp3');
        $this->assertEquals('track1.mp3', $file1->getContents());
    }

    public function testConstructDirectory(): void
    {
        $this->assertInstanceOf(File::class, new File('f1'));
    }

    /**
     * @depends testConstructDirectory
     */
    public function testDirectoryGetContentsWithoutElements(): void
    {
        $directory1 = new Directory('Black album');
        $this->assertEquals('Black album', $directory1->getContents());
    }

    /**
     * @depends testConstructFile
     * @depends testConstructDirectory
     */
    public function testDirectoryAddElement(): void
    {
        $file1 = new File('track1.mp3');
        $directory1 = new Directory('Black album');
        $this->assertTrue($directory1->addElement($file1));
    }

    /**
     * @depends testConstructFile
     * @depends testConstructDirectory
     */
    public function testDirectoryRemoveElement(): void
    {
        $file1 = new File('track1.mp3');
        $file2 = new File('track2.mp3');
        $file3 = new File('track3.mp3');
        $directory1 = new Directory('Black album');

        $this->assertTrue($directory1->addElement($file1));
        $this->assertTrue($directory1->addElement($file2));
        $this->assertTrue($directory1->addElement($file3));
        $this->assertTrue($directory1->removeElement($file2));
    }

    /**
     * @depends testConstructFile
     * @depends testConstructDirectory
     */
    public function testDirectoryRemoveNonExistingElement(): void
    {
        $file1 = new File('track1.mp3');
        $file2 = new File('track2.mp3');
        $file3 = new File('track3.mp3');
        $directory1 = new Directory('Black album');

        $this->assertTrue($directory1->addElement($file1));
        $this->assertTrue($directory1->addElement($file2));
        $this->assertFalse($directory1->removeElement($file3));
    }

    /*
     * @depends testDirectoryAddElement
     */
    public function testDirectoryGetContentsWithElements(): void
    {
        $file1 = new File('track1.mp3');
        $file2 = new File('track2.mp3');
        $file3 = new File('track3.mp3');
        $file4 = new File('track4.mp3');
        $directory1 = new Directory('Black album');
        $directory2 = new Directory('Metallica');

        $directory1->addElement($file1);
        $directory1->addElement($file2);
        $directory1->addElement($file3);

        $directory2->addElement($file4);
        $directory2->addElement($directory1);

        $this->assertEquals('Black album: track1.mp3 track2.mp3 track3.mp3', $directory1->getContents());
        $this->assertEquals('Metallica: track4.mp3 Black album: track1.mp3 track2.mp3 track3.mp3',
            $directory2->getContents());
    }
}
