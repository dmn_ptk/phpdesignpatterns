<?php


namespace PhpDesignPatterns\Behavioral\ChainOfResponsibilities;


interface Bundle
{
    public function getBundle(): array;
}