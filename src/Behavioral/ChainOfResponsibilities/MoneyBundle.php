<?php


namespace PhpDesignPatterns\Behavioral\ChainOfResponsibilities;


/**
 * Bundle of Money in different denominations
 */
class MoneyBundle implements Bundle
{

    /**
     * @var int
     */
    private $requestedMoney;

    /**
     * @var int
     */
    private $moneyToRelease;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var array
     */
    private $bundle;

    public function __construct(int $money, string $currency)
    {
        $this->requestedMoney = $money;
        $this->moneyToRelease = $money;
        $this->currency = $currency;
    }

    public function getMoneyToRelease(): int
    {
        return $this->moneyToRelease;
    }

    public function add(Money $money): void
    {
        if ($this->canAdd($money)) {
            $this->bundle[] = $money;
            $this->moneyToRelease -= $money->value * $money->count;
        }

    }

    private function canAdd(Money $money): bool
    {
        return ($money->currency === $this->currency)
            && ($money->value <= $this->getMoneyToRelease());
    }

    public function getBundle(): array
    {
        return $this->bundle;
    }
}