<?php


namespace PhpDesignPatterns\Behavioral\ChainOfResponsibilities;

/**
 * Part of ATM responsible for releasing certain amount of money in assigned denomination
 */
class MoneyStore implements ChainLink
{
    /**
     * @var Money
     */
    private $money;

    /**
     * @var ChainLink
     */
    private $nextLink;

    /**
     * @var MoneyBundle
     */
    private $moneyBundle;

    public function __construct(Money $money)
    {
        $this->money = $money;
    }

    public function setNextLink(ChainLink $link): void
    {
        $this->nextLink = $link;
    }

    public function hasLink(): bool
    {
        return !empty($this->nextLink);
    }

    public function release(Bundle $moneyBundle): Bundle
    {
        $this->moneyBundle = $moneyBundle;
        if($this->canPrepare()){
            $this->prepare();
        }

        if($this->canTriggerNextLink()){
            $this->nextLink->release($this->moneyBundle);
        }

        return $this->moneyBundle;
    }

    private function canPrepare(): bool
    {
        return $this->moneyBundle->getMoneyToRelease() / $this->money->value >= 1;
    }

    private function canTriggerNextLink(): bool
    {
        return $this->moneyBundle->getMoneyToRelease() > 0
            && $this->hasLink();
    }

    private function prepare()
    {
        $count = floor($this->moneyBundle->getMoneyToRelease() / $this->money->value);
        $moneyForBundle = new Money([
            'value' => $this->money->value,
            'count' => $count,
            'currency' => $this->money->currency
        ]);
        $this->moneyBundle->add($moneyForBundle);
    }
}