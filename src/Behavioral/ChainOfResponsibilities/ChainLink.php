<?php


namespace PhpDesignPatterns\Behavioral\ChainOfResponsibilities;


interface ChainLink
{
    public function setNextLink(ChainLink $link): void;
    public function hasLink(): bool;
    public function release(Bundle $bundle): Bundle;
}