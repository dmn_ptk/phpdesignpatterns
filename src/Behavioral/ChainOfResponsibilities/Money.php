<?php

namespace PhpDesignPatterns\Behavioral\ChainOfResponsibilities;

class Money
{

    /**
     * @var int
     */
    public $value;

    /**
     * @var int
     */
    public $count;

    /**
     * @var string
     */
    public $currency;
    public function __construct(array $money)
    {
        $this->value = (int) $money['value'];
        $this->count = empty($money['count']) ? 0 : (int) $money['count'];
        $this->currency = (string) $money['currency'];
    }
}