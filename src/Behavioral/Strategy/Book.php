<?php


namespace PhpDesignPatterns\Behavioral\Strategy;


class Book
{
    public $author;
    public $title;
    public $publisher;
    public $year;

    public function __construct(array $book)
    {
        $this->author = $book['author'];
        $this->title = $book['title'];
        $this->publisher = $book['publisher'];
        $this->year = $book['year'];
    }
}