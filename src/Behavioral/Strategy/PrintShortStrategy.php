<?php


namespace PhpDesignPatterns\Behavioral\Strategy;


class PrintShortStrategy extends Strategy
{

    public function execute(): string
    {
        return "{$this->book->author}: {$this->book->title}";
    }
}