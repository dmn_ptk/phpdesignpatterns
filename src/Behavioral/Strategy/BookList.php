<?php


namespace PhpDesignPatterns\Behavioral\Strategy;


class BookList
{

    /**
     * @var array
     */
    private $books;

    /**
     * @var Strategy
     */
    private $strategy;

    public function setBooks(array $books)
    {
        $this->books = $books;
    }

    public function count(): int
    {
        return count($this->books);
    }

    public function setStrategy(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    public function getStrategy(): Strategy
    {
        return $this->strategy;
    }

    public function returnList()
    {
        $list = [];
        foreach($this->books as $book){
            $this->strategy->setBook($book);
            $list[] = $this->strategy->execute();
        }

        return $list;
    }

}