<?php


namespace PhpDesignPatterns\Behavioral\Strategy;


class PrintFullStrategy extends Strategy
{
    public function execute(): string
    {
        return "{$this->book->author}: {$this->book->title}, {$this->book->publisher}, {$this->book->year}";
    }
}