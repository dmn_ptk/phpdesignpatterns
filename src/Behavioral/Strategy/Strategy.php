<?php


namespace PhpDesignPatterns\Behavioral\Strategy;


abstract class Strategy
{
    /**
     * @var Book
     */
    protected $book;

    public function setBook(Book $book)
    {
        $this->book = $book;
    }

    abstract function execute(): string;
}