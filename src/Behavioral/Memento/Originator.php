<?php
declare(strict_types=1);

namespace PhpDesignPatterns\Behavioral\Memento;


interface Originator
{
    public function createMemento(): CounterMemento;
    public function setMemento(CounterMemento $memento): void;
}