<?php
declare(strict_types=1);


namespace PhpDesignPatterns\Behavioral\Memento;


class Counter implements Originator
{


    /** @var int */
    private $state;

    /**
     * Counter constructor.
     * @param int $state
     */
    public function __construct(int $state = 0)
    {
        $this->state = $state;
    }

    public function createMemento(): CounterMemento
    {
        return new CounterMemento($this->getState());
    }

    public function setMemento(CounterMemento $memento): void
    {
        $this->state = $memento->getState();
    }

    public function count(): void
    {
        $this->state++;
    }

    public function getState(): int
    {
        return $this->state;
    }
}