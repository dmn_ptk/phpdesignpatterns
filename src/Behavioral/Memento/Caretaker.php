<?php
declare(strict_types=1);


namespace PhpDesignPatterns\Behavioral\Memento;


class Caretaker
{
    /** @var CounterMemento[] */
    private $mementoList;

    public function add(CounterMemento $memento): void
    {
        $this->mementoList[] = $memento;
    }

    public function get(int $index): CounterMemento
    {
        if(!isset($this->mementoList[$index])){
            throw new \Exception('Index out of range');
        }

        return $this->mementoList[$index];
    }

}