<?php
declare(strict_types=1);


namespace PhpDesignPatterns\Behavioral\Memento;


class CounterMemento implements Memento
{

    /**
     * @var int
     */
    private $state;

    public function __construct(int $state)
    {
        $this->state = $state;
    }

    public function getState(): int
    {
        return $this->state;
    }
}