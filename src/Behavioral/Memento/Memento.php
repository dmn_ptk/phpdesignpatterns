<?php
declare(strict_types=1);

namespace PhpDesignPatterns\Behavioral\Memento;


interface Memento
{
    public function getState();
}