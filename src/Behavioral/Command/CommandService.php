<?php


namespace PhpDesignPatterns\Behavioral\Command;

/**
 * Returns output produced from array of added and executed commands
 */
class CommandService
{
    /**
     * @var Broker
     */
    private $broker;

    /**
     * @var CommandFactory
     */
    private $factory;

    public function setBroker(Broker $broker): void
    {
        $this->broker = $broker;
    }

    public function setFactory(CommandFactory $factory): void
    {
        $this->factory = $factory;
    }

    public function addElementsFromEntities(array $entities): void
    {
        foreach ($entities as $entity) {
            $this->broker->add($this->factory::returnCommand($entity));
        }
    }

    public function count(): int
    {
        return $this->broker->count();
    }

    public function returnProcessedCommands()
    {
        return $this->broker->returnProcessed();
    }
}