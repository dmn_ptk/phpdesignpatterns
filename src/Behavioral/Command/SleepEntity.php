<?php


namespace PhpDesignPatterns\Behavioral\Command;


class SleepEntity implements Entity
{
    /**
     * @var int
     */
    private $duration;

    public function __construct(int $duration)
    {
        $this->duration = $duration;
    }

    public function getDuration() : int
    {
        return $this->duration;
    }
}