<?php


namespace PhpDesignPatterns\Behavioral\Command;

/**
 * Creates commands from entities
 */
interface CommandFactory
{
    public static function returnCommand(Entity $entity);
}