<?php


namespace PhpDesignPatterns\Behavioral\Command;


class SleepCard extends Card
{

    function execute() : array
    {
        return [
            'class' => $this->getEntityClass(),
            'desc' => $this->entity->getDuration()
        ];

    }
}