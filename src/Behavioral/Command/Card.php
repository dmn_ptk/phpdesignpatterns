<?php


namespace PhpDesignPatterns\Behavioral\Command;

/**
 * Encapsulate Entity in command
 */
abstract class Card implements Command
{
    /**
     * @var Entity
     */
    protected $entity;

    public function __construct(Entity $entity)
    {
        $this->entity = $entity;
    }

    protected function getEntityClass() : string
    {
        return get_class($this->entity);
    }
}