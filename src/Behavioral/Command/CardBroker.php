<?php


namespace PhpDesignPatterns\Behavioral\Command;


class CardBroker implements Broker
{

    /**
     * @var array
     */
    private $cards;

    public function add(Command $command): void
    {
        $this->cards[] = $command;
    }

    public function count(): int
    {
        return count($this->cards);
    }

    public function returnProcessed()
    {
        $processedCards = [];
        foreach($this->cards as $card){
            $processedCards[] = $card->execute();
        }

        return $processedCards;
    }
}