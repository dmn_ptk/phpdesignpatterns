<?php


namespace PhpDesignPatterns\Behavioral\Command;


class NappyCard extends Card
{

    function execute()
    {
        return [
            'class' => $this->getEntityClass(),
            'desc' => $this->entity->getType()
        ];
    }
}