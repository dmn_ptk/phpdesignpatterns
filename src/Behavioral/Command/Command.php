<?php


namespace PhpDesignPatterns\Behavioral\Command;


interface Command
{
    public function execute();
}