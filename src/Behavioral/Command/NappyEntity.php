<?php


namespace PhpDesignPatterns\Behavioral\Command;


class NappyEntity implements Entity
{
    const WET = 1;
    const DRY = 2;

    /**
     * @var int
     */
    private $type;

    public function __construct(int $type)
    {
        $this->type = $type;
    }

    public function getType() : int
    {
        return $this->type;
    }
}