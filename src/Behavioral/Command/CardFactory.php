<?php


namespace PhpDesignPatterns\Behavioral\Command;


class CardFactory implements CommandFactory
{

    public static function returnCommand(Entity $entity)
    {
        $entityExploded = explode('Entity', get_class($entity));
        $classNamespace = array_shift($entityExploded) . 'Card';
        $classNamespaceExploded = explode('\\', $classNamespace);
        $class = array_pop($classNamespaceExploded);
        $filename = __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';

        if(!file_exists($filename)){
            throw new \Exception("No file {$class}.php");
        }

        if(!class_exists($classNamespace)){
            throw new \Exception("No class {$class}");
        }

        require_once $filename;
        return new $classNamespace($entity);
    }
}