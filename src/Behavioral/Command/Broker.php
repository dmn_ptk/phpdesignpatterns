<?php


namespace PhpDesignPatterns\Behavioral\Command;

/**
 * Operates on commands
 */
interface Broker
{
    public function add(Command $command): void;

    public function returnProcessed();

    public function count(): int;
}