<?php


namespace PhpDesignPatterns\Behavioral\Mediator;


interface Mediator
{

    /**
     * Gets response from targeted client
     */
    public function getResponse(Request $request): Response;

    /**
     * Puts client to array
     */
    public function setClient(Client $client): void;
}