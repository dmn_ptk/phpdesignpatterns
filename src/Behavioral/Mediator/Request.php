<?php


namespace PhpDesignPatterns\Behavioral\Mediator;


class Request
{
    public function __construct(array $request)
    {
        $this->senderType = $request['senderType'];
        $this->data = $request['data'];
        $this->receiverType = $request['receiverType'];
    }
    /**
     * @var string
     */
    public $senderType;

    /**
     * @var string
     */
    public $receiverType;

    /**
     * @var string
     */
    public $data;
}