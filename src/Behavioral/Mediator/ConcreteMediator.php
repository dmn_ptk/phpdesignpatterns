<?php


namespace PhpDesignPatterns\Behavioral\Mediator;


class ConcreteMediator implements Mediator
{
    /**
     * @var array
     */
    private $clients;

    public function setClient(Client $client): void
    {
        $this->clients[$client->getType()] = $client;
    }

    public function getResponse(Request $request): Response
    {
        if($this->hasClient($request->receiverType)){
            return $this->clients[$request->receiverType]->sentResponse($request);
        }else{
            throw new \OutOfRangeException("No client '{$request->receiverType}' added");
        }
    }

    private function hasClient(string $type): bool
    {
        return isset($this->clients[$type]);
    }

    public function getClient(string $type): Client
    {
        if($this->hasClient($type)){
            return $this->clients[$type];
        }else{
            throw new \OutOfRangeException("No client '{$type}' added");
        }
    }

}