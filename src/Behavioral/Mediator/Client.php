<?php


namespace PhpDesignPatterns\Behavioral\Mediator;


interface Client
{
    public function setMediator(Mediator $mediator): void;
    public function sendRequest(string $receiverType): void;

    /**
     * Sends response to Mediator
     */
    public function sentResponse(Request $request): Response;

    /**
     * Returns type of client
     */
    public function getType(): string;
}