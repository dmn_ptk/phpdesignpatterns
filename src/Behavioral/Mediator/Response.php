<?php


namespace PhpDesignPatterns\Behavioral\Mediator;


class Response
{
    public function __construct(array $response)
    {
        $this->senderType = $response['senderType'];
        $this->data = $response['data'];
        $this->receiverType = $response['receiverType'];
    }
    /**
     * @var string
     */
    public $senderType;

    /**
     * @var string
     */
    public $receiverType;

    /**
     * @var string
     */
    public $data;

    /**
     * @var Request
     */
    public $request;
}