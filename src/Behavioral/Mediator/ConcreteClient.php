<?php


namespace PhpDesignPatterns\Behavioral\Mediator;


class ConcreteClient implements Client
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var Mediator
     */
    private $mediator;

    /**
     * @var Response
     */
    private $response;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function setMediator(Mediator $mediator): void
    {
        $this->mediator = $mediator;
    }

    public function sendRequest(string $receiverType): void
    {
        $request = new Request([
            'senderType' => $this->getType(),
            'receiverType' => $receiverType,
            'data' => $this->getType()
        ]);
        $this->response = $this->mediator->getResponse($request);
    }

    public function getResponse(): Response
    {
        return $this->response;
    }

    public function sentResponse(Request $request): Response
    {
        return new Response([
            'senderType' => $this->getType(),
            'receiverType' => $request->senderType,
            'data' => $request->data . $this->getType()
        ]);
    }

    public function getType(): string
    {
        return $this->type;
    }
}