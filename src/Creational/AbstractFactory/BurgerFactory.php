<?php


namespace PhpDesignPatterns\Creational\AbstractFactory;


class BurgerFactory extends AbstractFactory
{
    const CLASSIC = 1;
    const CHEESE = 2;
    public function getMenuItem(int $item) : MenuItem
    {
        if(!in_array($item, [self::CLASSIC, self::CHEESE])){
            throw new \Exception("Not existing item selected");
        }

        switch ($item) {
            case 1:
                return new BurgerClassic();
            case 2:
                return new BurgerCheese();
        }
    }
}