<?php


namespace PhpDesignPatterns\Creational\AbstractFactory;

class BurgerClassic implements MenuItem
{
    public function getName() : string
    {
        return 'Classic Burger';
    }
}
