<?php

namespace PhpDesignPatterns\Creational\AbstractFactory;

interface MenuItem
{
    public function getName() : string;
}
