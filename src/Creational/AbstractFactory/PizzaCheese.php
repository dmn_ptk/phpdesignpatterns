<?php

namespace PhpDesignPatterns\Creational\AbstractFactory;

class PizzaCheese implements MenuItem
{
    public function getName() : string
    {
        return 'Cheese';
    }
}
