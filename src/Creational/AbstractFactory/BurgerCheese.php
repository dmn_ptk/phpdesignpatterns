<?php


namespace PhpDesignPatterns\Creational\AbstractFactory;


class BurgerCheese implements MenuItem
{
    public function getName() : string
    {
        return 'Cheese Burger';
    }
}