<?php

namespace PhpDesignPatterns\Creational\AbstractFactory;

class PizzaFactory extends AbstractFactory
{
    const MARGHERITA = 1;
    const CHEESE = 2;
    public function getMenuItem(int $item) : MenuItem
    {
        if(!in_array($item, [self::MARGHERITA, self::CHEESE])){
            throw new \Exception("Not existing item selected");
        }

        switch ($item) {
            case 1:
                return new PizzaMargherita();
            case 2:
                return new PizzaCheese();
        }
    }
}
