<?php


namespace PhpDesignPatterns\Creational\AbstractFactory;

abstract class AbstractFactory
{
    const PIZZA = 1;
    const BURGER = 2;

    public static function getFactory(int $type)
    {
        switch ($type) {
            case 1:
                return new PizzaFactory();
            case 2:
                return new BurgerFactory();
        }
    }

    abstract public function getMenuItem(int $item): MenuItem;
}
