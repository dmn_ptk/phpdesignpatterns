<?php

namespace PhpDesignPatterns\Creational\AbstractFactory;

class PizzaMargherita implements MenuItem
{
    public function getName() : string
    {
        return 'Margherita';
    }
}
