<?php

namespace PhpDesignPatterns\Creational\Prototype;

interface Product
{
    public function setTitle(string $title) : void;

    public function getTitle() : string;

    public function __clone();
}
