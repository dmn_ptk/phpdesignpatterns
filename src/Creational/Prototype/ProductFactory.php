<?php


namespace PhpDesignPatterns\Creational\Prototype;


class ProductFactory
{
    protected $products;
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function getProduct(string $hash = '') : Product
    {
        if(!isset($this->products[$hash])){
            throw new \Exception('Product not exists');
        }

        return $this->products[$hash];
    }
}