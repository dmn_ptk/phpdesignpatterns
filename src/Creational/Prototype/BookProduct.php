<?php


namespace PhpDesignPatterns\Creational\Prototype;

class BookProduct implements Product
{
    protected $title;
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function __clone()
    {
    }
}
