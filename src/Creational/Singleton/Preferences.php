<?php

namespace PhpDesignPatterns\Creational\Singleton;

class Preferences
{
    protected static $instance;
    protected $options = [];

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function getInstance() : Preferences
    {
        if(static::$instance === null){
            return new Preferences();
        }
        return static::$instance;

    }

    public function setOption(string $key, $value) : void
    {
        $this->options[$key] = $value;
    }

    public function getOption($key)
    {
        return $this->options[$key];
    }
}