<?php

namespace PhpDesignPatterns\Creational\Factory;

class PizzaCheese implements MenuItem
{
    public function getName() : string
    {
        return 'Cheese';
    }
}
