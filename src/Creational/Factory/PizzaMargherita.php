<?php

namespace PhpDesignPatterns\Creational\Factory;

class PizzaMargherita implements MenuItem
{
    public function getName() : string
    {
        return 'Margherita';
    }
}
