<?php

namespace PhpDesignPatterns\Creational\Factory;

interface MenuItem
{
    public function getName() : string;
}
