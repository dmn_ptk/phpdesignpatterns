<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 08.04.18
 * Time: 20:37
 */

namespace PhpDesignPatterns\Structural\Adapter;


class NewMessenger
{
    public function returnMessageArray(): array
    {
        return [
            'msg' => 'Some message in an array',
            'status' => 'ok'
        ];
    }
}