<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 08.04.18
 * Time: 20:36
 */

namespace PhpDesignPatterns\Structural\Adapter;


class OldMessenger implements Messenger
{
    public function showMessage(): string
    {
        return 'Some string message';
    }
}