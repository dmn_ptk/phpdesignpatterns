<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 08.04.18
 * Time: 20:33
 */

namespace PhpDesignPatterns\Structural\Adapter;


class MessengerAdapter implements Messenger
{
    /**
     * @var NewMessenger
     */
    private $messenger;

    public function __construct(NewMessenger $messenger)
    {
        $this->messenger = $messenger;
    }

    public function showMessage(): string
    {
        $response = $this->messenger->returnMessageArray();
        return $response['msg'];
    }
}