<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 08.04.18
 * Time: 20:33
 */

namespace PhpDesignPatterns\Structural\Adapter;


interface Messenger
{
    public function showMessage(): string;
}