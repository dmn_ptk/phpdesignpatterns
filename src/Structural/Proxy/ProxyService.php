<?php
declare(strict_types=1);

namespace PhpDesignPatterns\Structural\Proxy;


class ProxyService implements Service
{
    /**
     * @var Service
     */
    private $remoteService;

    public function __construct(Service $remote)
    {
        $this->remoteService = $remote;
    }

    public function generateReport(): array
    {
        return $this->remoteService->generateReport();
    }
}