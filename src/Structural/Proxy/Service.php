<?php
declare(strict_types=1);

namespace PhpDesignPatterns\Structural\Proxy;


interface Service
{
    public function generateReport(): array;
}