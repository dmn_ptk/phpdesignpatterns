<?php


namespace PhpDesignPatterns\Structural\Composite;

class File implements Component
{
    protected $name = '';

    public function __construct(string $name = "")
    {
        $this->name = $name;
    }

    public function getContents(): string
    {
        return $this->name;
    }
}
