<?php


namespace PhpDesignPatterns\Structural\Composite;

interface Component
{
    public function getContents(): string;
}
