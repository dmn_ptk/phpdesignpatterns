<?php


namespace PhpDesignPatterns\Structural\Composite;

class Directory implements Component
{
    protected $name = '';
    protected $elements = [];

    public function __construct(string $name = "")
    {
        $this->name = $name;
    }

    public function getContents(): string
    {
        $result = $this->name;
        if (!empty($this->elements)) {
            $result .= ':';

            foreach ($this->elements as $el) {
                $result .= ' ' . $el->getContents();
            }
        }

        return $result;
    }

    public function addElement(Component $el = null): bool
    {
        if (empty($el)) {
            return false;
        }

        $this->elements[] = $el;
        return true;
    }

    public function removeElement(Component $el = null): bool
    {
        if (empty($el)) {
            return false;
        }

        foreach ($this->elements as $k => $v) {
            if ($v === $el) {
                unset($this->elements[$k]);
                return true;
            }
        }

        return false;
    }
}
